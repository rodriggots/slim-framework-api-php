<?php

namespace App\controllers;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use App\models\Db;

class UserController
{
    public function getAll(Request $req, Response $res):Response
    {
        $sql = "SELECT & FROM customers";

        try{
           $db = new Db(); 
           $conn = $db->connect();
           $stmt = $conn->query($sql);
           $customers = $stmt->fetchAll(PDO::FETCH_OBJ);
           $db = null;

           $res->getBody()->write(json_encode($customers));
           return $res 
               ->withHeader('Content-Type', 'application/json')
               ->withStatus(200);
        }catch(PDOException $e){
            $error = [
                "message" => $e->getMessage()
            ];
        };

        $res->getBody()->write(json_encode($error));
        return $res 
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(500);
    }
}
