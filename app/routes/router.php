<?php

use Slim\App;

use App\controllers\UserController;

return function(App $app) {
    $app->get('/', [UserController::class, 'getAll']);
};
