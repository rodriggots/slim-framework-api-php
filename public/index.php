<?php

require_once __DIR__ . '/../vendor/autoload.php';

use Slim\Factory\AppFactory;


$app = AppFactory::create();
$app->addErrorMiddleware(true, true, true);



$router = require_once '../app/routes/router.php';
$router($app);

$app->run();
